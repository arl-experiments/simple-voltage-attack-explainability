import logging
from typing import Dict

from palaestrai.agent.memory import Memory
from palaestrai.agent.objective import Objective
from .gauss import normal_distribution_pdf

LOG = logging.getLogger("palaestrai.agent.objective")


class VoltageBandViolationPendulum(Objective):
    VM_PU_LOW = 0.85
    VM_PU_HIGH = 1.15
    SIGMA = -0.05
    C = -10.0
    A = -12.0

    def __init__(self, params: Dict):
        super().__init__(params)
        self._our_sensors = params.get("sensors")
        self._sigma = params.get("sigma", VoltageBandViolationPendulum.SIGMA)
        self._c = params.get("c", VoltageBandViolationPendulum.C)
        self._a = params.get("a", VoltageBandViolationPendulum.A)
        self.step = 0

    def objective_from_vm(self, vm_pu: float):
        objective = normal_distribution_pdf(
            x=vm_pu,
            mu=1.0,
            sigma=self._sigma,
            c=self._c,
            a=self._a
        )
        objective += normal_distribution_pdf(
            x=vm_pu,
            mu=0.83,
            sigma=0.01,
            c=0.0,
            a=self._a
        )
        objective += normal_distribution_pdf(
            x=vm_pu,
            mu=1.16,
            sigma=0.01,
            c=0.0,
            a=self._a
        )
        return objective

    def internal_reward(self, memory: Memory, **kwargs):
        """Expect Voltage values and reward voltage band violations."""
        # Calc voltages:

        if self._our_sensors:
            sensors = [
                agent_sensor
                for our_sensor in self._our_sensors
                for agent_sensor in memory.get_rewards().columns
                if our_sensor in agent_sensor and "vm_pu" in our_sensor
            ]
            rewards = memory.get_rewards()[-1:][sensors]
        else:
            rewards = memory.get_rewards()[-1:].filter(
                like=".vm_pu", axis=1
            )

        vm_pu = float(rewards.median(axis=1).iloc[0])
        objective = self.objective_from_vm(vm_pu)

        # Calc constraint violations:

        if self._our_sensors:
            sensors = [
                agent_sensor
                for our_sensor in self._our_sensors
                for agent_sensor in memory.get_rewards().columns
                if our_sensor in agent_sensor and "in_service" in our_sensor
            ]
            objective += (
                -10 * float(memory.get_rewards()[-1:][sensors].sum(axis=1).iloc[0]) #- len(memory.get_rewards()[-1:][sensors].columns))
            )

        self.step += 1
        return objective


class SimpleVoltageBandPendulumDefender(Objective):
    VM_PU_LOW = 0.98
    VM_PU_HIGH = 1.02
    SIGMA = 0.032
    C = 0.0
    A = 10.0

    def __init__(self, params: Dict):
        params = {} if not params else params
        super().__init__(params)
        self._our_sensors = params.get("sensors")
        self._sigma = params.get("sigma", SimpleVoltageBandPendulumDefender.SIGMA)
        self._c = params.get("c", SimpleVoltageBandPendulumDefender.C)
        self._a = params.get("a", SimpleVoltageBandPendulumDefender.A)
        self.step = 0

    def objective_from_vm(self, vm_pu: float):
        objective = normal_distribution_pdf(
            x=vm_pu,
            mu=1.0,
            sigma=0.032,
            c=0.0,
            a=10.0
        )
        return objective

    def internal_reward(self, memory: Memory, **kwargs):
        # Calc voltages:

        if self._our_sensors:
            sensors = [
                agent_sensor
                for our_sensor in self._our_sensors
                for agent_sensor in memory.get_rewards().columns
                if our_sensor in agent_sensor
            ]
            rewards = memory.get_rewards()[-1:][sensors]
        else:
            rewards = memory.get_rewards()[-1:].filter(
                like=".vm_pu", axis=1
            )
        vm_pu = float(rewards.median(axis=1).iloc[0])
        objective = self.objective_from_vm(vm_pu)

        # Calc constraint violations:

        if self._our_sensors:
            sensors = [
                agent_sensor
                for our_sensor in self._our_sensors
                for agent_sensor in memory.get_rewards().columns
                if our_sensor in agent_sensor and "in_service" in our_sensor
            ]
            objective += (
                -10 * (
                    float(memory.get_rewards()[-1:][sensors].sum(axis=1).iloc[0])
                    - len(memory.get_rewards()[-1:][sensors].columns))
            )

        self.step += 1
        return objective
