import pandapower as pp
from pandapower import pandapowerNet
from midas.modules.powergrid.custom import midasmv

def build_grid():

    net: pandapowerNet = midasmv.build_grid()

    pp.runpp(net)

    return net
