# Overview

The both agents (attacker and defender), that are trained with ddpg, have their models plotted as dts.

The tar plots are with the invariants x,y in [0.8,1.2]
and the vm_pu_range are plotted with the invariants x,y in [0.92,1.02] (visually extracted min/max values from vm_pu plot of voltage magnitude from sensors)

The DTs are plotted with NN2EQCDT with the commit 4a92b89f0687a6a2ea2bec8c9bde479bbfdb0831
