import os
from tempfile import TemporaryDirectory

from palaestrai.core import RuntimeConfig
from palaestrai.store import Session
import palaestrai.store.query as palq

import sys
import logging

stream_handler = logging.StreamHandler(sys.stdout)
from palaestrai.store.query_debug_utils import LOG

LOG.setLevel(logging.DEBUG)
LOG.addHandler(stream_handler)


def extract_simulation_data():
    _tmpdir = TemporaryDirectory()
    RuntimeConfig().reset()


    RuntimeConfig().load(
        {
            "store_uri":
                f"postgresql://palaestrai:TreasureKeeperOfTheOrderOfARL"
                f"@localhost:5432/carl"
        }
    )

    _dbh = Session()


    try:
        experiment_name = "Dummy Experiment record for ExperimentRun Reactive Power (only one line) Experiment"
        experiment_run_uid = "Reactive Power (only one line) Experiment"
        #experiment_run_instance_uids = [
        # "f6eed64b-669d-47a9-bb37-1f07fd0dcc14"]
        experiment_run_phase_uids = ["Testing"]
        agent_uids = ["SAC Attacker"]

        erc = palq.muscle_action_values_non_empty_multi_index(
            _dbh,
            experiment_name=experiment_name,
            experiment_run_uid=experiment_run_uid,
            #experiment_run_instance_uids=experiment_run_instance_uids,
            experiment_run_phase_uids=experiment_run_phase_uids,
            agent_uids=agent_uids,
        )
        test_sim_data_path = os.path.join(os.path.dirname(__file__),
                     'test_simulation_data_carl_one_line_attacker'
                     '.json')

        erc.to_json(
            str(test_sim_data_path),
            orient="split", indent=2)
    finally:
        LOG.removeHandler(stream_handler)

if __name__ == "__main__":
    extract_simulation_data()