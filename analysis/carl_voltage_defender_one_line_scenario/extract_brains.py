import os
from tempfile import TemporaryDirectory

import torch

from palaestrai.core import RuntimeConfig
from palaestrai.store import Session
import palaestrai.store.query as palq

import sys
import logging

stream_handler = logging.StreamHandler(sys.stdout)
from palaestrai.store.query_debug_utils import LOG

LOG.setLevel(logging.DEBUG)
LOG.addHandler(stream_handler)

def save_brain(agent_uid, brain_state_tag, brain_state_state):
    brains_dir = os.path.join(os.path.dirname(__file__), "brains")
    brain_file = os.path.join(brains_dir, agent_uid + "_" + brain_state_tag
                              + ".bin")
    torch.save(brain_state_state, brain_file)

def extract_brains():
    _tmpdir = TemporaryDirectory()
    RuntimeConfig().reset()


    RuntimeConfig().load(
        {
            "store_uri":
                f"postgresql://palaestrai:TreasureKeeperOfTheOrderOfARL"
                f"@localhost:5432/carl"
        }
    )

    _dbh = Session()


    try:
        experiment_name = "Dummy Experiment record for ExperimentRun Reactive Power (only one line) Experiment"
        experiment_run_uid = "Reactive Power (only one line) Experiment"
        #experiment_run_instance_uids = [
        # "f6eed64b-669d-47a9-bb37-1f07fd0dcc14"]
        experiment_run_phase_uids = ["Testing"]
        agent_uids = ["SAC Attacker", "SAC Defender"]

        erc = palq.latest_brain_state(
            _dbh,
            experiment_name=experiment_name,
            experiment_run_uid=experiment_run_uid,
            experiment_run_phase_uids=experiment_run_phase_uids,
            agent_uids=agent_uids,
        )
        erc.apply(lambda x: save_brain(x["agent_uid"], x["brain_state_tag"], x["brain_state_state"]), axis=1)
    finally:
        LOG.removeHandler(stream_handler)

if __name__ == "__main__":
    extract_brains()