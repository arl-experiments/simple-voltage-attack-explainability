import_mosaik
import numpy as np

seed_max = 1000000
output_path = "_outputs"
data_path = "/root/.config/midas/midas_data"
step_size = 60
start_date = "2017-05-05 00:00:00+0100"
end = 864000
cos_phi = 0.9
no_db = False
with_timesim = False
with_arl = True
with_ict = False
no_rng = False
forecast_horizon_hours = 0.25
flexibility_horizon_hours = 0.25
flexibility_horizon_start_hours = 0
cmd = "python"
default_weather_name = "WeatherBre2009-2020.hdf5"
default_simbench_name = "1-LV-rural3--0-sw.hdf5"
default_commcercials_name = "CommercialsRefTMY3.hdf5"
rng = np.random.RandomState(None)
store_database_sim_params = {'sim_name': 'MidasStore', 'step_size': 60, 'buffer_size': 0, 'keep_old_files': True}
database_params = {'filename': '/workspace/_outputs/midas_store-endgame-defender_mv.hdf5', 'buffer_size': 0, 'keep_old_files': True}
powergrid_attack_mv_sim_params = {'gridfile': 'simple_attack_explainability_paper.simple_pn_line:build_grid', 'use_constraints': True, 'constraints': [['load', 0.02], ['sgen', 0.05], ['line', 100]], 'sim_name': 'Powergrid', 'step_size': 60, 'grid_params': {}, 'plotting': False, 'plot_path': '/workspace/_outputs/plots', 'save_grid_json': False, 'grid_mapping': {}}
powergrid_attack_mv_params = {'gridfile': 'simple_attack_explainability_paper.simple_pn_line:build_grid', 'grid_params': {}, 'use_constraints': True, 'constraints': [['load', 0.02], ['sgen', 0.05], ['line', 100]]}
comdata_attack_mv_sim_params = {'interpolate': True, 'randomize_data': True, 'randomize_cos_phi': True, 'mapping': {4: [['Hospital', 2.0]]}, 'sim_name': 'CommercialData', 'step_size': 60, 'grid_name': 'attack_mv', 'start_date': '2017-05-05 00:00:00+0100', 'data_path': '/root/.config/midas/midas_data', 'cos_phi': 0.9, 'seed_max': 1000000, 'seed': 414088, 'filename': 'CommercialsRefTMY3.hdf5'}
comdata_attack_mv_hospital_4_0_params = {'scaling': 2.0}
pwdata_attack_mv_sim_params = {'grid_name': 'attack_mv', 'peak_mapping': {5: [['Wind', 2]]}, 'sim_name': 'PVWindData', 'step_size': 60, 'start_date': '2017-05-05 00:00:00+0100', 'data_path': '/root/.config/midas/midas_data', 'cos_phi': 0.9, 'interpolate': False, 'randomize_data': False, 'randomize_cos_phi': False, 'noise_factor': 0.2, 'sgen_scaling': 1.0, 'scale_mapping': {}, 'seed_max': 1000000, 'seed': 377737, 'filename': 'GenWindPV50Hertz.hdf5'}
pwdata_attack_mv_Wind_5_0_params = {'p_peak_mw': 2.0}
sensors = [{'sensor_id': 'Powergrid-0.0-bus-1.vm_pu', 'observation_space': 'Box(low=0.8, high=1.2, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-1.va_degree', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-1.p_mw', 'observation_space': 'Box(low=0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-1.q_mvar', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-2.vm_pu', 'observation_space': 'Box(low=0.8, high=1.2, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-2.va_degree', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-2.p_mw', 'observation_space': 'Box(low=0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-2.q_mvar', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-3.vm_pu', 'observation_space': 'Box(low=0.8, high=1.2, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-3.va_degree', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-3.p_mw', 'observation_space': 'Box(low=0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-3.q_mvar', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-4.vm_pu', 'observation_space': 'Box(low=0.8, high=1.2, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-4.va_degree', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-4.p_mw', 'observation_space': 'Box(low=0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-4.q_mvar', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-5.vm_pu', 'observation_space': 'Box(low=0.8, high=1.2, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-5.va_degree', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-5.p_mw', 'observation_space': 'Box(low=0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-bus-5.q_mvar', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-ext_grid-0.p_mw', 'observation_space': 'Box(low=0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-ext_grid-0.q_mvar', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-line-0.loading_percent', 'observation_space': 'Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-line-0.in_service', 'observation_space': 'Discrete(2)'}, {'sensor_id': 'Powergrid-0.0-line-1.loading_percent', 'observation_space': 'Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-line-1.in_service', 'observation_space': 'Discrete(2)'}, {'sensor_id': 'Powergrid-0.0-line-2.loading_percent', 'observation_space': 'Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-line-2.in_service', 'observation_space': 'Discrete(2)'}, {'sensor_id': 'Powergrid-0.0-line-3.loading_percent', 'observation_space': 'Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-line-3.in_service', 'observation_space': 'Discrete(2)'}, {'sensor_id': 'Powergrid-0.0-load-0-4.p_mw', 'observation_space': 'Box(low=0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-load-0-4.q_mvar', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-load-0-4.in_service', 'observation_space': 'Discrete(2)'}, {'sensor_id': 'Powergrid-0.0-load-1-5.p_mw', 'observation_space': 'Box(low=0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-load-1-5.q_mvar', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-load-1-5.in_service', 'observation_space': 'Discrete(2)'}, {'sensor_id': 'Powergrid-0.0-sgen-0-2.p_mw', 'observation_space': 'Box(low=0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-sgen-0-2.q_mvar', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-sgen-0-2.in_service', 'observation_space': 'Discrete(2)'}, {'sensor_id': 'Powergrid-0.0-sgen-1-3.p_mw', 'observation_space': 'Box(low=0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-sgen-1-3.q_mvar', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-sgen-1-3.in_service', 'observation_space': 'Discrete(2)'}, {'sensor_id': 'Powergrid-0.0-sgen-2-5.p_mw', 'observation_space': 'Box(low=0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-sgen-2-5.q_mvar', 'observation_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.0-sgen-2-5.in_service', 'observation_space': 'Discrete(2)'}, {'sensor_id': 'Powergrid-0.0-trafo-0.loading_percent', 'observation_space': 'Box(low=0.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.Grid-0.health', 'observation_space': 'Box(low=0, high=1.2, shape=(1,), dtype=np.float32)'}, {'sensor_id': 'Powergrid-0.Grid-0.grid_json', 'observation_space': 'Box(low=0, high=1, shape=(1,), dtype=np.float32)'}]
actuators = [{'actuator_id': 'Powergrid-0.0-load-0-4.p_mw', 'action_space': 'Box(low=0, high=0.0, shape=(1,), dtype=np.float32)'}, {'actuator_id': 'Powergrid-0.0-load-0-4.q_mvar', 'action_space': 'Box(low=0, high=0.0, shape=(1,), dtype=np.float32)'}, {'actuator_id': 'Powergrid-0.0-load-1-5.p_mw', 'action_space': 'Box(low=0, high=0.0, shape=(1,), dtype=np.float32)'}, {'actuator_id': 'Powergrid-0.0-load-1-5.q_mvar', 'action_space': 'Box(low=0, high=0.0, shape=(1,), dtype=np.float32)'}, {'actuator_id': 'Powergrid-0.0-sgen-0-2.p_mw', 'action_space': 'Box(low=0, high=0.0, shape=(1,), dtype=np.float32)'}, {'actuator_id': 'Powergrid-0.0-sgen-0-2.q_mvar', 'action_space': 'Box(low=-1.5, high=1.5, shape=(1,), dtype=np.float32)'}, {'actuator_id': 'Powergrid-0.0-sgen-1-3.p_mw', 'action_space': 'Box(low=0, high=0.0, shape=(1,), dtype=np.float32)'}, {'actuator_id': 'Powergrid-0.0-sgen-1-3.q_mvar', 'action_space': 'Box(low=-2.0, high=2.0, shape=(1,), dtype=np.float32)'}, {'actuator_id': 'Powergrid-0.0-sgen-2-5.p_mw', 'action_space': 'Box(low=-1.0, high=1.0, shape=(1,), dtype=np.float32)'}, {'actuator_id': 'Powergrid-0.0-sgen-2-5.q_mvar', 'action_space': 'Box(low=0, high=0.0, shape=(1,), dtype=np.float32)'}, {'actuator_id': 'Powergrid-0.0-trafo-0.tap_pos', 'action_space': 'Box(low=-10, high=10, shape=(1,), dtype=np.int32)'}]

sim_config = {'MidasStore': {'python': 'midas.modules.store.simulator:MidasHdf5'}, 'Powergrid': {'python': 'midas.modules.powergrid.simulator:PandapowerSimulator'}, 'CommercialData': {'python': 'midas.modules.comdata.simulator:CommercialDataSimulator'}, 'PVWindData': {'python': 'midas.modules.pwdata.simulator:PVWindDataSimulator'}}

world = mosaik.World(sim_config)
store_database_sim = world.start(**store_database_sim_params)
powergrid_attack_mv_sim = world.start(**powergrid_attack_mv_sim_params)
comdata_attack_mv_sim = world.start(**comdata_attack_mv_sim_params)
pwdata_attack_mv_sim = world.start(**pwdata_attack_mv_sim_params)

database = store_database_sim.Database(**database_params)
powergrid_attack_mv = powergrid_attack_mv_sim.Grid(**powergrid_attack_mv_params)
powergrid_attack_mv_bus_1 = [e for e in powergrid_attack_mv.children if e.eid == "0-bus-1"][0]
powergrid_attack_mv_bus_2 = [e for e in powergrid_attack_mv.children if e.eid == "0-bus-2"][0]
powergrid_attack_mv_bus_3 = [e for e in powergrid_attack_mv.children if e.eid == "0-bus-3"][0]
powergrid_attack_mv_bus_4 = [e for e in powergrid_attack_mv.children if e.eid == "0-bus-4"][0]
powergrid_attack_mv_bus_5 = [e for e in powergrid_attack_mv.children if e.eid == "0-bus-5"][0]
powergrid_attack_mv_ext_grid_0 = [e for e in powergrid_attack_mv.children if e.eid == "0-ext_grid-0"][0]
powergrid_attack_mv_line_0 = [e for e in powergrid_attack_mv.children if e.eid == "0-line-0"][0]
powergrid_attack_mv_line_1 = [e for e in powergrid_attack_mv.children if e.eid == "0-line-1"][0]
powergrid_attack_mv_line_2 = [e for e in powergrid_attack_mv.children if e.eid == "0-line-2"][0]
powergrid_attack_mv_line_3 = [e for e in powergrid_attack_mv.children if e.eid == "0-line-3"][0]
powergrid_attack_mv_load_0_4 = [e for e in powergrid_attack_mv.children if e.eid == "0-load-0-4"][0]
powergrid_attack_mv_load_1_5 = [e for e in powergrid_attack_mv.children if e.eid == "0-load-1-5"][0]
powergrid_attack_mv_sgen_0_2 = [e for e in powergrid_attack_mv.children if e.eid == "0-sgen-0-2"][0]
powergrid_attack_mv_sgen_1_3 = [e for e in powergrid_attack_mv.children if e.eid == "0-sgen-1-3"][0]
powergrid_attack_mv_sgen_2_5 = [e for e in powergrid_attack_mv.children if e.eid == "0-sgen-2-5"][0]
powergrid_attack_mv_trafo_0 = [e for e in powergrid_attack_mv.children if e.eid == "0-trafo-0"][0]
comdata_attack_mv_hospital_4_0 = comdata_attack_mv_sim.Hospital(**comdata_attack_mv_hospital_4_0_params)
pwdata_attack_mv_Wind_5_0 = pwdata_attack_mv_sim.Wind(**pwdata_attack_mv_Wind_5_0_params)

world.connect(powergrid_attack_mv_bus_1, database, *['vm_pu', 'va_degree', 'p_mw', 'q_mvar'], **{})
world.connect(powergrid_attack_mv_bus_2, database, *['vm_pu', 'va_degree', 'p_mw', 'q_mvar'], **{})
world.connect(powergrid_attack_mv_bus_3, database, *['vm_pu', 'va_degree', 'p_mw', 'q_mvar'], **{})
world.connect(powergrid_attack_mv_bus_4, database, *['vm_pu', 'va_degree', 'p_mw', 'q_mvar'], **{})
world.connect(powergrid_attack_mv_bus_5, database, *['vm_pu', 'va_degree', 'p_mw', 'q_mvar'], **{})
world.connect(powergrid_attack_mv_ext_grid_0, database, *['p_mw', 'q_mvar'], **{})
world.connect(powergrid_attack_mv_line_0, database, *['loading_percent', 'in_service'], **{})
world.connect(powergrid_attack_mv_line_1, database, *['loading_percent', 'in_service'], **{})
world.connect(powergrid_attack_mv_line_2, database, *['loading_percent', 'in_service'], **{})
world.connect(powergrid_attack_mv_line_3, database, *['loading_percent', 'in_service'], **{})
world.connect(powergrid_attack_mv_load_0_4, database, *['p_mw', 'q_mvar', 'in_service'], **{})
world.connect(powergrid_attack_mv_load_1_5, database, *['p_mw', 'q_mvar', 'in_service'], **{})
world.connect(powergrid_attack_mv_sgen_0_2, database, *['p_mw', 'q_mvar', 'in_service'], **{})
world.connect(powergrid_attack_mv_sgen_1_3, database, *['p_mw', 'q_mvar', 'in_service'], **{})
world.connect(powergrid_attack_mv_sgen_2_5, database, *['p_mw', 'q_mvar', 'in_service'], **{})
world.connect(powergrid_attack_mv_trafo_0, database, *['loading_percent'], **{})
world.connect(powergrid_attack_mv, database, *['health'], **{})
world.connect(comdata_attack_mv_hospital_4_0, powergrid_attack_mv_load_0_4, *['p_mw', 'q_mvar'], **{})
world.connect(comdata_attack_mv_hospital_4_0, database, *['p_mw', 'q_mvar'], **{})
world.connect(pwdata_attack_mv_Wind_5_0, powergrid_attack_mv_sgen_2_5, *['p_mw', 'q_mvar'], **{})
world.connect(pwdata_attack_mv_Wind_5_0, database, *['p_mw', 'q_mvar'], **{})

world.run(until=end)

