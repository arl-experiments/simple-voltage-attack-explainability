import json
import os

import pandas as pd
import yaml

from palaestrai.experiment import ExperimentRun


def convert_index(json_obj):
    to_tuples = [tuple(i) if isinstance(i, list) else i for i in json_obj]
    if all(isinstance(i, list) for i in json_obj):
        return pd.MultiIndex.from_tuples(to_tuples)
    else:
        return pd.Index(to_tuples)


def extend_only_zero_len_intervals(x):
    min_val, max_val = x['min'], x['max']
    MIN_INTERVAL_ADD_SIZE = 10 ** (-5)
    if max_val-min_val < MIN_INTERVAL_ADD_SIZE:
        d = {
            'min': min_val - MIN_INTERVAL_ADD_SIZE,
            'max': max_val + MIN_INTERVAL_ADD_SIZE
        }
        return pd.Series(data=d, index=['min', 'max'])
    else:
        return x

def extend_interval(x):
    min_val, max_val = x['min'], x['max']
    extend_len = 0 #(max_val - min_val)
    MIN_INTERVAL_ADD_SIZE = 10 ** (-5)
    d = {
        'min': min_val - extend_len - MIN_INTERVAL_ADD_SIZE,
        'max': max_val + extend_len + MIN_INTERVAL_ADD_SIZE
    }
    return pd.Series(data=d, index=['min', 'max'])


# Source: https://github.com/pandas-dev/pandas/issues/4889#issuecomment-925520769
def deserialize_multiindex_dataframe(dataframe_json: str) -> pd.DataFrame:
    """
    Deserialize the dataframe json into a dataframe object.

    The dataframe json must be generated with DataFrame.to_json(orient="split")
    This function is to address the fact that `pd.read_json()` isn't behaving
    correctly (yet)
    https://github.com/pandas-dev/pandas/issues/4889
    """
    json_dict = json.loads(dataframe_json)
    columns = convert_index(json_dict['columns'])
    index = convert_index(json_dict['index'])
    dataframe = pd.DataFrame(json_dict["data"], index, columns)
    return dataframe


def reorder_data(
        erc: pd.DataFrame,
        column: str,
        reordered_idx_list: list
) -> pd.DataFrame:
    ercT = erc.T

    idx_df = (ercT.index.to_frame()
              .drop([column], level=0))

    sensors_idx_df = pd.MultiIndex.from_product(
        [[column], reordered_idx_list]).to_frame()
    idx_df = pd.concat([idx_df, sensors_idx_df])
    idx_df = pd.MultiIndex.from_frame(idx_df)
    erc = ercT.reindex(idx_df).T

    return erc


def extract_intervals():
    test_sim_data_path = os.path.join(os.path.dirname(__file__),
                                      'test_simulation_data_carl_defender.json')

    with open(str(test_sim_data_path), "r") as test_sim_json:
        erc = deserialize_multiindex_dataframe(test_sim_json.read())

    # ticks [0:600] have high variation, [600:] are rel. constant
    # erc = erc.loc[erc[("muscle_action_simtime_ticks",
    # "muscle_action_simtime_ticks")]>600]

    erc_sensor_readings_min_max = (
        erc.muscle_sensor_readings.describe().transpose()[
            ['min', 'max']
        ]
    )

    erc_sensor_readings_min_max = erc_sensor_readings_min_max.apply(
        extend_interval, axis=1,
        result_type="expand")

    #erc_sensor_readings_min_max = erc_sensor_readings_min_max.apply(
    #    extend_interval, axis=1,
    #    result_type="expand")

#    exp_run_path = os.path.abspath(os.path.join(
#        os.path.dirname(__file__),
#        '../../palaestrai-runfiles/voltage_defender.yml'))
#    exp_run = ExperimentRun.load(exp_run_path)
#    sensors = exp_run.canonical_config["schedule"][0]["Testing"][
#        'agents'][0]['sensors']
#    actuators = exp_run.canonical_config["schedule"][0]["Testing"][
#        'agents'][0]['actuators']

    #erc_sensor_readings_min_max = erc_sensor_readings_min_max.reindex(
    #    sensors)
    #
    #sensor_readings_min_max_path = os.path.join(os.path.dirname(__file__),
    #                                            'sensor_readings_min_max.json')
    #erc_sensor_readings_min_max.to_json(
    #    str(sensor_readings_min_max_path),
    #    orient="split", indent=2)

    idx_pos_interval_dict = {
        "constraints": {
            i: [float(erc_sensor_readings_min_max.iloc[i]['min']),
                float(erc_sensor_readings_min_max.iloc[i]['max'])] for i in
            range(len(erc_sensor_readings_min_max))
        }
    }

    constraints = os.path.join(os.path.dirname(__file__),
                               'constraints.yml')

    with open(str(constraints), 'w') as outfile:
        yaml.safe_dump(idx_pos_interval_dict, outfile,
                       default_flow_style=None)

    #erc = reorder_data(erc, "muscle_sensor_readings", sensors)
    #erc = reorder_data(erc, "muscle_actuator_setpoints", actuators)

#    test_sim_data_path = os.path.join(os.path.dirname(__file__),
#                                      'test_simulation_data.json')
#
#    erc.to_json(
#        str(test_sim_data_path),
#        orient="split", indent=2)


if __name__ == "__main__":
    extract_intervals()
