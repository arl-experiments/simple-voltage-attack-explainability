podman kill simple-voltage-attack-explainability || true
podman rm simple-voltage-attack-explainability || true

#podman build -f docker/Dockerfile . -t simple-voltage-attack-explainability

podman run $(/opt/pkm/bin/pmgpu.sh) -e ROOTLESS=1 --name simple-voltage-attack-explainability -v /home/tlogemann/simple-voltage-attack-explainability/docker/demonstrator-start.sh:/carl/demonstrator-start.sh -v /home/tlogemann/simple-voltage-attack-explainability/docker/start.sh:/palaestrai/containers/start.sh -v /home/tlogemann/simple-voltage-attack-explainability/docker/midas_data:/root/.config/midas/midas_data -v /home/tlogemann/simple-voltage-attack-explainability:/workspace simple-voltage-attack-explainability "/bin/bash /carl/demonstrator-start.sh" >output.log
